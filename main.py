import numpy as np
import tensorflow.keras
import tensorflow as tf
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score
from tensorflow.keras.callbacks import Callback
import matplotlib.pyplot as plt
from tensorflow.keras.callbacks import Callback
from tensorflow.keras.utils import normalize
import matplotlib.pyplot as plt
import warnings
import logging
from tensorflow.keras.layers import Embedding, Input, Dense, Concatenate
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.models import Model

#cechy numeryczne
numeric = [0,2,4,10,11,12]
#liczba cech
no_feats = 14
#rozmiar cech
feat_sizes = [0,8,0,16,0,7,14,6,5,2,0,0,0,41]
workclass = ["Private", "Self-emp-not-inc", "Self-emp-inc", "Federal-gov", "Local-gov", "State-gov", "Without-pay", "Never-worked"]
education = ["Bachelors", "Some-college", "11th", "HS-grad", "Prof-school", "Assoc-acdm", "Assoc-voc", "9th", "7th-8th", "12th", "Masters", "1st-4th", "10th", "Doctorate", "5th-6th", "Preschool"]
marital_status = ["Married-civ-spouse", "Divorced", "Never-married", "Separated", "Widowed", "Married-spouse-absent", "Married-AF-spouse"]
occupation = ["Tech-support", "Craft-repair", "Other-service", "Sales", "Exec-managerial", "Prof-specialty", "Handlers-cleaners", "Machine-op-inspct", "Adm-clerical", "Farming-fishing", "Transport-moving", "Priv-house-serv", "Protective-serv", "Armed-Forces"]
relationship = ["Wife", "Own-child", "Husband", "Not-in-family", "Other-relative", "Unmarried"]
race = ["White", "Asian-Pac-Islander", "Amer-Indian-Eskimo", "Other", "Black"]
sex = ["Female", "Male"]
native_country = ["United-States", "Cambodia", "England", "Puerto-Rico", "Canada", "Germany", "Outlying-US(Guam-USVI-etc)", "India", "Japan", "Greece", "South", "China", "Cuba", "Iran", "Honduras", "Philippines", "Italy", "Poland", "Jamaica", "Vietnam", "Mexico", "Portugal", "Ireland", "France", "Dominican-Republic", "Laos", "Ecuador", "Taiwan", "Haiti", "Columbia", "Hungary", "Guatemala", "Nicaragua", "Scotland", "Thailand", "Yugoslavia", "El-Salvador", "Trinadad&Tobago", "Peru", "Hong", "Holand-Netherlands"]

feats_name = [workclass, education, marital_status, occupation, relationship, race, sex, native_country]
name_data = 'combined_adult.data'

def translate_to_vector(position_od_one, length_vector):
    vec = np.zeros(length_vector)
    vec[position_od_one] = 1
    return vec

def empty_sample():

        return np.array([ np.zeros(i).T if i!=0 else 0 for i in feat_sizes])
def parse_line(line):
    sample = empty_sample()
    tmp = 0
    tokens = line.split(', ')
    for i,token in enumerate(tokens[0:-1]):
        if i not in numeric:
            vec_name_feats = feats_name[tmp]
            index = vec_name_feats.index(token)
            vec = translate_to_vector(index,len(vec_name_feats))
            tmp += 1
            sample[i] = vec

        else:
            sample[i] = int(token)
    if '<=50K' in tokens[-1]:
        y = 1
    else: #tokens[-1] == '>50K':
        y = 0

    return(sample, y)

def get_data(file):
    X = np.empty((0,no_feats))
    Y = np.empty((0))
    for line in open(file):
        if '?' in line:
            continue
        (x,y) = parse_line(line)

        X = np.append(X,[x],axis=0)
        Y = np.append(Y,[y],axis=0)
    return (X,Y)

def format_data(X,Y,rng, feats = [i for i in range(no_feats)]):
    input_data = []

    for i in range(no_feats):
        if i in feats:
            if i in numeric:
                norm = np.linalg.norm(X[:,i])
                input_data.append([x for x in X[rng,i]/norm])
            else:
                input_data.append([x for x in X[rng,i]])

    Y_categorical = np.zeros((0,2))
    for y in Y[rng]:
        if y == 0:
            Y_categorical = np.append(Y_categorical,[np.array([0,1])],axis=0)
        else:
            Y_categorical = np.append(Y_categorical,[np.array([1,0])],axis=0)
    return (input_data,Y_categorical)
    from tensorflow.keras.layers import Embedding, Input, Dense, Concatenate
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.models import Model

def get_model(feat_nos = [i for i in range(no_feats)]):
    feats = []
    numeric_feats = []
    for feat in range(no_feats):
        if feat in feat_nos:
            if feat not in numeric:
                feats.append(Input(feat_sizes[feat],name='feat'+str(feat)))
            else:
                feats.append(Input(1,name='featNum'+str(feat)))
    deep_nets = []
    iter = 0
    for feat in range(no_feats):
        if feat in feat_nos:
            if feat not in numeric:
                deep_nets.append(Dense(2,activation='relu',use_bias=True)(feats[iter]))
            else:
                deep_nets.append(Dense(1,activation='linear',use_bias=True)(feats[iter]))
            iter += 1
    conc = Concatenate()(deep_nets)
    deep_in = conc
    h1 = Dense(30,activation='relu',use_bias=True)(deep_in)
    h2 = Dense(20,activation='relu',use_bias=True)(h1)
    h3 = Dense(10,activation='relu',use_bias=True)(h2)
    final = Dense(2,activation='softmax')(h3)
    feat1 = Input(feat_sizes[0])
    result = Dense(2)(feat1)

    model = Model(inputs=feats, outputs=final)
    model.compile(loss='binary_crossentropy',metrics = ['accuracy'],optimizer = Adam())
    return model


class onEpochEndCallback(Callback):
    def __init__(self, no_epochs):
        self.no_epochs = no_epochs
        self.epochs_passed = 0
        self.history_loss = []
        self.history_accuracy = []
        self.val_history_loss = []
        self.val_history_accuracy = []

    def on_epoch_end(self,epoch,logs = {}):
        self.epochs_passed += 1
        self.history_loss.append(logs['loss'])
        self.history_accuracy.append(logs['acc'])
        self.val_history_loss.append(logs['val_loss'])
        self.val_history_accuracy.append(logs['val_acc'])
        if self.epochs_passed % self.no_epochs == 0:
            print('Epoch: %d\n\tTrain Accuracy:\t%.2f%%\tValidation Accuracy\t%.2f%%\n\tTrain Loss\t%.2f\tValidation Loss:\t%.2f' %
                  (self.epochs_passed,logs['acc'],logs['val_acc'],logs['loss'],logs['val_loss']))



logging.getLogger('boto').setLevel(logging.WARNING)
logging.getLogger('keras').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)


warnings.filterwarnings('ignore')

seed = 7
validation_split = 0.1

(X,Y) = get_data('combined_adult.data')
loggers = []
kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=seed)
cvscores = []
n_fold = 0
for train, test in kfold.split(X, Y):
    n_fold += 1
    print('---- Fold  %d ----' % n_fold)
    callback = onEpochEndCallback(100)
    features = [i for i in range(no_feats)]
    model = get_model(feat_nos = features)
    (input_train, output_train) = format_data(X,Y,train,feats = features)
    (input_test, output_test) = format_data(X,Y,test, feats = features)
    print(model.summary)
    model.fit(input_train,np.array(output_train),epochs=400, batch_size=32, verbose = 1, validation_split = validation_split, callbacks = [callback])
    predicts_train = model.predict(input_train)
    score_train = roc_auc_score(output_train, predicts_train)
    predicts = model.predict(input_test)
    score = roc_auc_score(output_test,predicts)
    print("%s %.2f%%" % ('Train data AUC value for that fold: ', score_train*100))
    print("%s %.2f%%" % ('Test data AUC value for that fold: ', score*100))
    print
    cvscores.append(score*100)
    loggers.append(callback)
print("5 fold AUC (2 sigma/95%% confidence): %.2f%% (+/- %.2f%%)" % (np.mean(cvscores), 2*np.std(cvscores)))


plt.figure(figsize = (15,20))
plt.subplot(421)
plt.ylabel('Training loss')
plt.title('Training loss')
plt.xlabel('Epoch')
plt.ylim([0,1.5])
for i in range(5):
    plt.plot(loggers[i].history_loss)
plt.legend(['Fold 1','Fold 2','Fold 3','Fold 4','Fold 5'])
import matplotlib.pyplot as plt

plt.figure(figsize = (15,20))
plt.subplot(421)
plt.ylabel('Training loss')
plt.title('Training loss')
plt.xlabel('Epoch')
plt.ylim([0,1.5])
for i in range(5):
    plt.plot(loggers[i].history_loss)
plt.legend(['Fold 1','Fold 2','Fold 3','Fold 4','Fold 5'])

plt.subplot(422)
avg_loss = np.sum([loggers[i].history_loss for i in range(5)], axis = 0)/5
plt.ylim([0,1.5])
plt.xlabel('Epoch')
plt.title('Averaged training loss')
plt.plot(avg_loss)

plt.subplot(423)
plt.ylabel('Validation loss')
plt.title('Validation loss')
plt.xlabel('Epoch')
plt.ylim([0,1.5])
for i in range(5):
    plt.plot(loggers[i].val_history_loss)
plt.legend(['Fold 1','Fold 2','Fold 3','Fold 4','Fold 5'])

plt.subplot(424)
avg_val_loss = np.sum([loggers[i].val_history_loss for i in range(5)], axis = 0)/5
plt.ylim([0,1.5])
plt.xlabel('Epoch')
plt.title('Averaged validation loss')
plt.plot(avg_val_loss)

plt.subplot(425)
plt.ylabel('Training accuracy')
plt.title('Training accuracy')
plt.xlabel('Epoch')
plt.ylim([0,1])
for i in range(5):
    plt.plot(loggers[i].history_accuracy)
plt.legend(['Fold 1','Fold 2','Fold 3','Fold 4','Fold 5'])

plt.subplot(426)
avg_acc = np.sum([loggers[i].history_accuracy for i in range(5)], axis = 0)/5
plt.ylim([0,1])
plt.xlabel('Epoch')
plt.title('Averaged training accuracy')
plt.plot(avg_acc)

plt.subplot(427)
plt.ylabel('Validation accuracy')
plt.title('Validation accuracy')
plt.xlabel('Epoch')
plt.ylim([0,1])
for i in range(5):
    plt.plot(loggers[i].val_history_accuracy)
plt.legend(['Fold 1','Fold 2','Fold 3','Fold 4','Fold 5'])

plt.subplot(428)
avg_val_acc = np.sum([loggers[i].val_history_accuracy for i in range(5)], axis = 0)/5
plt.ylim([0,1])
plt.xlabel('Epoch')
plt.title('Averaged validation loss')
plt.plot(avg_val_acc)



plt.subplot(422)
avg_loss = np.sum([loggers[i].history_loss for i in range(5)], axis = 0)/5
plt.ylim([0,1.5])
plt.xlabel('Epoch')
plt.title('Averaged training loss')
plt.plot(avg_loss)

plt.subplot(423)
plt.ylabel('Validation loss')
plt.title('Validation loss')
plt.xlabel('Epoch')
plt.ylim([0,1.5])
for i in range(5):
    plt.plot(loggers[i].val_history_loss)
plt.legend(['Fold 1','Fold 2','Fold 3','Fold 4','Fold 5'])

plt.subplot(424)
avg_val_loss = np.sum([loggers[i].val_history_loss for i in range(5)], axis = 0)/5
plt.ylim([0,1.5])
plt.xlabel('Epoch')
plt.title('Averaged validation loss')
plt.plot(avg_val_loss)

plt.subplot(425)
plt.ylabel('Training accuracy')
plt.title('Training accuracy')
plt.xlabel('Epoch')
plt.ylim([0,1])
for i in range(5):
    plt.plot(loggers[i].history_accuracy)
plt.legend(['Fold 1','Fold 2','Fold 3','Fold 4','Fold 5'])

plt.subplot(426)
avg_acc = np.sum([loggers[i].history_accuracy for i in range(5)], axis = 0)/5
plt.ylim([0,1])
plt.xlabel('Epoch')
plt.title('Averaged training accuracy')
plt.plot(avg_acc)

plt.subplot(427)
plt.ylabel('Validation accuracy')
plt.title('Validation accuracy')
plt.xlabel('Epoch')
plt.ylim([0,1])
for i in range(5):
    plt.plot(loggers[i].val_history_accuracy)
plt.legend(['Fold 1','Fold 2','Fold 3','Fold 4','Fold 5'])

plt.subplot(428)
avg_val_acc = np.sum([loggers[i].val_history_accuracy for i in range(5)], axis = 0)/5
plt.ylim([0,1])
plt.xlabel('Epoch')
plt.title('Averaged validation loss')


plt.savefig('result.jpg', dpi=300, quality=80, optimize=True, progressive=True, bbox_inches='tight')
